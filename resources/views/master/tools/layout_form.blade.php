<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Find Me | Solusi Cari Barang Hilang</title>
    <meta name="twitter:description" content="Anda kehilangan barang? Coba cek di Find Me aja!">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:image" content="{{asset('img/logo.png')}}">
    <meta property="og:type" content="website">
    <meta name="description" content="Anda kehilangan barang? Coba cek di Find Me aja!">
    <meta property="og:image" content="{{asset('img/logo.png')}}">
    <meta name="twitter:title" content="Find Me | Solusi Cari Barang Hilang">
    <link rel="apple-touch-icon" href="{{asset('img/logo.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/logo.png')}}">
    <link rel="icon" type="image/png" sizes="186x180" href="{{asset('img/logo.png')}}">
    <link rel="icon" type="image/png" sizes="186x180" href="{{asset('img/logo.png')}}">
    <link rel="icon" type="image/png" sizes="186x180" href="{{asset('img/logo.png')}}">
    <link rel="icon" type="image/png" sizes="186x180" href="{{asset('img/logo.png')}}">
    <link rel="icon" type="image/png" sizes="186x180" href="{{asset('img/logo.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    @include('master.tools.css')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 1-column blank-page blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column" style="background:url({{asset('img/bg.jpg')}}); background-repeat: no-repeat; background-size: cover;">
    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>


    @include('master.tools.js')
    @include('alert')
    
</body>
<!-- END: Body-->

</html>