<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @isset(Auth::user()->id)
                @if(Auth::user()->role_id==1)
                <li class=" navigation-header"><span>MENU</span><i class=" feather icon-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
                </li>
                <li class="nav-item {{ (request()->routeIs('master.master')) ? 'active' : '' }}"><a href="{{route('master.master')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard"> Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Database</span></a>
            <ul class="menu-content">
                <li class="{{ (request()->routeIs('master.data_master')) ? 'active' : '' }}"><a class="menu-item" href="{{route('master.data_master')}}" data-i18n="Master">Master</a>
                </li>
                   <li class="{{ (request()->routeIs('pengguna.data_pengguna')) ? 'active' : '' }}"><a class="menu-item" href="{{route('pengguna.data_pengguna')}}" data-i18n="Master">pengguna</a>
                </li>
                </ul>
                </li> 
                <li class=" nav-item"><a href="#"><i class="fas fa-boxes"></i><span class="menu-title" data-i18n="Dashboard">Barang</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->routeIs('barang.data_barang')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.data_barang')}}" data-i18n="Master">Data Barang</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_null')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_null')}}" data-i18n="Master">Belum diklaim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_klaim')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_klaim')}}" data-i18n="Master">Diklaim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_tolak')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_tolak')}}" data-i18n="Master">Ditolak</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_konfirmasi')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_konfirmasi')}}" data-i18n="Master">Dikonfirmasi</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_kirim')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_kirim')}}" data-i18n="Master">Dikirim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_terima')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_terima')}}" data-i18n="Master">Diterima</a>
                        </li>
                    </ul>
                </li>
                </ul>
                </li>
                    <!-- End Menu Akun -->
                @endif
                @if(Auth::user()->role_id==2)
                    @include('pengguna.tools.side')
                @endif
                @endisset
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->