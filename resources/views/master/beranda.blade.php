@extends('master.tools.layout')

@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Stats -->
                <div class="row">
                <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-danger bg-darken-2">
                                    <i class="icon-user font-large-2 white"></i>
                                    </div>
                                    <div class="p-2 bg-gradient-x-danger white media-body">
                                        <h5>Pengguna</h5>
                                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-up"></i>{{$pengguna}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-info bg-darken-2">
                                    <i class="icon-basket-loaded font-large-2 white"></i>
                                    </div>
                                    <div class="p-2 bg-gradient-x-info white media-body">
                                        <h5>Barang</h5>
                                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-up"></i>{{$barang}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-success bg-darken-2">
                                    <i class="icon-basket-loaded font-large-2 white"></i>
                                    </div>
                                    <div class="p-2 bg-gradient-x-success white media-body">
                                        <h5>Barang Di Klaim</h5>
                                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-up"></i>{{$klaim_barang}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-warning bg-darken-2">
                                        <i class="icon-basket-loaded font-large-2 white"></i>
                                    </div>
                                    <div class="p-2 bg-gradient-x-warning white media-body">
                                        <h5>Log Barang</h5>
                                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-down"></i>{{$log_barang}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!--/ Stats -->
            </div>
        </div>
</div>
<!-- END: Content-->
@endsection