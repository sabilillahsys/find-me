@extends('layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">FOTO BARANG</i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row" style="padding-bottom:10px;">
                                            <div class="col-lg-12 text-center mb-3">
                                            <img src="{{url('/').Storage::url($data->pic1)}}" class="img-fluid" alt="Responsive image" width="300" height="400">
                                            </div>
                                            <button type="button" data-toggle="modal" data-target="#tambah" class="btn btn-block btn-info">Klaim Barang</button>
                                    <!-- Modal-->
                                    <div id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                        <div role="document" class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-body">
                                            <form action="{{route('klaim_barang.input_klaim_barang')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label>Bukti *</label>
                                                    <input type="file" name="bukti" class="form-control" placeholder="Nama Lengkap" required>
                                                    <input type="hidden" name="barang_id" class="form-control" value="{{$data->id}}"  required> 
                                                </div>
                                                <div class="form-group">
                                                    <label>Deskripsi *</label>
                                                    <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi" required>
                                                </div>
                                                <div class="form-group">       
                                                    <input type="submit" value="Klaim" class="btn btn-block btn-primary">
                                                </div>
                                            </form> 
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                        <!-- end of modal -->
                                            <div class="col-lg-12 text-center">
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">DETAIL BARANG</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-12">
                                    <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Nama Barang</td>
                                                    <td>:</td>
                                                    <td>{{$data->nama_barang}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Deskripsi Barang</td>
                                                    <td>:</td>
                                                    <td>{{$data->deskripsi_barang}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Ditemukan</td>
                                                    <td>:</td>
                                                    <td>{{$data->tanggal}}</td>
                                                </tr>
                                                <tr>
                                                    <td >Kota</td>
                                                    <td >:</td>
                                                    <td>{{$data->kota}}</td>
                                                </tr>
                                                <tr>
                                                    <td >Kecamatan</td>
                                                    <td >:</td>
                                                    <td>{{$data->kecamatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td >Alamat</td>
                                                    <td >:</td>
                                                    <td>{{$data->alamat}}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">DETAIL PENEMU</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-12">
                                    <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Nama Penemu</td>
                                                    <td>:</td>
                                                    <td>{{$data->user->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Kelamin</td>
                                                    <td>:</td>
                                                    <td>{{$data->user->jk}}</td>
                                                </tr>
                                                <tr>
                                                    <td>No Hp</td>
                                                    <td>:</td>
                                                    <td><a href="https://wa.me/{{$data->user->hp}}">{{$data->user->hp}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td >Kota</td>
                                                    <td >:</td>
                                                    <td>{{$data->user->kota}}</td>
                                                </tr>
                                                <tr>
                                                    <td >Kecamatan</td>
                                                    <td >:</td>
                                                    <td>{{$data->user->kecamatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td >Alamat</td>
                                                    <td >:</td>
                                                    <td>{{$data->user->alamat}}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">LOG BARANG</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                <div class="row" style="padding-bottom:10px;">
                                    <div class="col-md-12">
                                    <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Tanggal</td>
                                                    <td>:</td>
                                                    <td>{{$data->tanggal}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Resi</td>
                                                    <td>:</td>
                                                    <td>{{$data->resi}}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
@endpush