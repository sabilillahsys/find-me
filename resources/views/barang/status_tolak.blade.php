@extends('layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Barang Tolak<i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                        </div>
                                            <div class="container table-responsive">                        
                                            <table class="table table-striped table-bordered" id="data-pelanggan">
                                            <thead>
                                                <tr>
                                                <th></th>
                                                    <th>No.</th>
                                                    <th>Nama Penemu</th>
                                                    <th>Nama Barang</th>
                                                </tr>
                                            </thead>
                                            </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="1" cellspacing="0" border="0">'+
        '<tr>'+
            '<td colspan="2">'+d.lokasi+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Status</td>'+
            '<td>'+d.status+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kota</td>'+
            '<td>'+d.kota+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kecamatan</td>'+
            '<td>'+d.kecamatan+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Alamat</td>'+
            '<td>'+d.alamat+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>harga</td>'+
            '<td>'+d.harga+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 1</td>'+
            '<td>'+d.pic1+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 2</td>'+
            '<td>'+d.pic2+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 3</td>'+
            '<td>'+d.pic3+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('barang.json_tolak')}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'penemu', name: 'penemu' },
            { data: 'nama_barang', name: 'nama_barang'},
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});

    

</script>
<script>
function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
//   alert("Copied the text: " + copyText.value);
  $(document).ready(function(){ 
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: "success",
            title: "Link Disalin"
            })
     }); 
}
</script>
<script>
$(document).ready(function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
});
var lt = null;
var lg = null;
function showPosition(position) {
    lt = position.coords.latitude; 
    lg = position.coords.longitude;
$('#lt').val(lt);
$('#lg').val(lg);
}
</script>
@endpush