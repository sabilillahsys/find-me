@extends('layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
            <!-- Enable - disable FixedHeader table -->
            <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Log Barang</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                    <form action="{{route('log_barang.input_log_barang')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                    <div class="row">
                                                    <div class="form-group">
                                                        <input type="hidden" name="barang_id" class="form-control"  required value="{{$id}}">
                                                        </div>
                                                    <div class="col-md-6">
                                                            <label><b>Tanggal *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="date" name="tanggal" class="form-control" placeholder="dd/mm/yyyy" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Resi *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="text" name="resi" class="form-control" placeholder="Resi" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Status *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="text" name="status" class="form-control" placeholder="Status" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Gambar *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="file" name="pic" class="form-control"  required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                <div class="form-group">       
                                                    <button type="submit" class="btn btn-primary btn-block">Tambah</button>
                                                </div>
                                            </form> 
                                    </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data LOG Barang</i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                        </div>
                                            <div class="container table-responsive">                        
                                            <table class="table table-striped table-bordered" id="data-pelanggan">
                                            <thead>
                                                <tr>
                                                <th></th>
                                                    <th>No.</th>
                                                    <th>Nama Barang</th>
                                                    <th>Resi</th>
                                                </tr>
                                            </thead>
                                            </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    '<tr>'+
            '<td>Aksi:</td>'+
            '<td>'+d.action+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Tanggal</td>'+
            '<td>'+d.tanggal+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Status</td>'+
            '<td>'+d.status+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar</td>'+
            '<td>'+d.pic+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('log_barang.json_log_barang',$id)}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'barang', name: 'barang'},
            { data: 'resi', name: 'resi' },
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});

    

</script>
<script>
function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
//   alert("Copied the text: " + copyText.value);
  $(document).ready(function(){ 
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: "success",
            title: "Link Disalin"
            })
     }); 
}
</script>
@endpush