@extends('layout')

@section('content')
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
            @if(Auth::User()->role_id==1)
            <!-- Enable - disable FixedHeader table -->
            <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"><i class="fas fa-boxes"></i> Masukkan Barang</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                    <form action="{{route('barang.input_barang')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                                <label><b>Nama Penemu *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <select class="form-control" name="user_id" id="pengguna"></select>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Nama Barang *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Deskripsi Barang *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                            <input type="text" name="deskripsi_barang" class="form-control" placeholder="Deskripsi Barang" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Kota *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="kota" placeholder="Kota" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Kecamatan *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Alamat *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="alamat" placeholder="Alamat" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Tanggal *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="date" class="form-control" name="tanggal" placeholder="" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>LT *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="lt" placeholder="LT" id="lt" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>LG *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="lg" placeholder="LG" id="lg" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Harga *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="text" class="form-control" name="harga" placeholder="Harga" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Gambar 1 *</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="file" class="form-control" name="pic1" placeholder="Gambar 1" required>
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Gambar 2 (Opsional)</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="file" class="form-control" name="pic2" placeholder="Gambar 2">
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Gambar 3 (Opsional)</b></label>
                                                            <fieldset class="form-group position-relative has-icon-left">
                                                                <input type="file" class="form-control" name="pic3" placeholder="Gambar 3">
                                                                <div class="form-control-position">
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                <div class="form-group">       
                                                    <button type="submit" class="btn btn-primary btn-block">Tambah</button>
                                                </div>
                                            </form> 
                                    </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
                @endif
                <section id="fixedheader">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Data Barang <i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i></h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                                        </ul>
                                    </div>
                                        </div>
                                            <div class="container table-responsive">                        
                                            <table class="table table-striped table-bordered" id="data-pelanggan">
                                            <thead>
                                                <tr>
                                                <th></th>
                                                    <th>No.</th>
                                                    <th>Nama Penemu</th>
                                                    <th>Nama Barang</th>
                                                </tr>
                                            </thead>
                                            </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Enable - disable FixedHeader table -->
            </div>
        </div>
    </div>
<!-- END: Content-->
@endsection
@push('scripts')
<script>
//fucktion awal
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="1" cellspacing="0" border="0">'+
    @if(Auth::User()->role_id==1)
    '<tr>'+
            '<td colspan="2">'+d.action+'</td>'+
        '</tr>'+
        @endif
        '<tr>'+
            '<td colspan="2">'+d.klaim_barang+' '+d.log_barang+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td colspan="2">'+d.lokasi+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kota</td>'+
            '<td>'+d.kota+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kecamatan</td>'+
            '<td>'+d.kecamatan+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Alamat</td>'+
            '<td>'+d.alamat+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>harga</td>'+
            '<td>'+d.harga+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 1</td>'+
            '<td>'+d.pic1+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 2</td>'+
            '<td>'+d.pic2+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Gambar 3</td>'+
            '<td>'+d.pic3+'</td>'+
        '</tr>'+
    '</table>';
}
//end function
$(document).ready(function() {
    var table = $('#data-pelanggan').DataTable({
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        ajax: "{{route('barang.json_barang')}}",
        columns: [
            {"className": 'details-control',"orderable": false,"data": null,"defaultContent": ''},
            { data: 'DT_RowIndex', name: 'DT_RowIndex'},
            { data: 'penemu', name: 'penemu' },
            { data: 'nama_barang', name: 'nama_barang'},
        ],
    });
// Add event listener for opening and closing details
    $('#data-pelanggan tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });
// Batas bawah
});

    

</script>
<script>
function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
//   alert("Copied the text: " + copyText.value);
  $(document).ready(function(){ 
    const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: "success",
            title: "Link Disalin"
            })
     }); 
}
</script>
<script type="text/javascript">
  $('#pengguna').select2({
    placeholder: 'Nama',
    ajax: {
      url: '{{route("pengguna.api")}}',
      dataType: 'json',
      delay: 250,
      data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.name,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

</script>
<script>
$(document).ready(function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
});
var lt = null;
var lg = null;
function showPosition(position) {
    lt = position.coords.latitude; 
    lg = position.coords.longitude;
$('#lt').val(lt);
$('#lg').val(lg);
}
</script>
@endpush