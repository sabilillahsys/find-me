<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" navigation-header"><span>MENU</span><i class=" feather icon-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
                </li>
                <li class="nav-item {{ (request()->routeIs('pengguna.pengguna')) ? 'active' : '' }}"><a href="{{route('pengguna.pengguna')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard"> Dashboard</span></a>
                </li>   
                <li class="nav-item {{ (request()->routeIs('klaim_barang.data_user_klaim_barang')) ? 'active' : '' }}"><a href="{{route('klaim_barang.data_user_klaim_barang')}}"><i class="fas fa-hands-helping"></i><span class="menu-title" data-i18n="Dashboard"> Klaim Barang</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="fas fa-boxes"></i><span class="menu-title" data-i18n="Dashboard">Barang Temuan</span></a>
                    <ul class="menu-content">
                        <li class="{{ (request()->routeIs('barang.data_user_barang')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.data_user_barang')}}" data-i18n="Master">Input Barang</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_null_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_null_user')}}" data-i18n="Master">Belum diklaim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_klaim_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_klaim_user')}}" data-i18n="Master">Diklaim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_tolak_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_tolak_user')}}" data-i18n="Master">Ditolak</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_konfirmasi_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_konfirmasi_user')}}" data-i18n="Master">Dikonfirmasi</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_kirim_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_kirim_user')}}" data-i18n="Master">Dikirim</a>
                        </li>
                        <li class="{{ (request()->routeIs('barang.status_terima_user')) ? 'active' : '' }}"><a class="menu-item" href="{{route('barang.status_terima_user')}}" data-i18n="Master">Diterima</a>
                        </li>
                    </ul>
                </li>
                </li>
<!-- End Menu Akun -->
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->