@extends('layout')

@section('content')
 <!-- BEGIN: Content-->
 <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
               <!-- First Row [Prosucts]-->

                <h2 class="font-weight-bold mb-2">Barang Temuan</h2>
                <p class="font-italic text-muted mb-2">Silahkan cari barang anda, pastikan anda memiliki bukti sebelum klaim</p>
                <div class="row mb-1 ml-2">
                    <form action="{{route('pengguna.pengguna')}}" method="get" class="form-inline">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" placeholder="Search" name="q">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="row">
                    @foreach($barang as $barangs)
                    <div class="col-lg-3 col-md-6 d-flex ">
                        <!-- Card-->
                        <div class="card rounded shadow-sm border-0">
                            <div class="card-body flex-fill"><img src="{{url('/').Storage::url($barangs->pic1)}}" class="img-responsive" width="200px" min-height="200px" alt="{{$barangs->nama_barang}}">
                                <h5> <a href="#" class="text-dark">{{$barangs->nama_barang}}</a></h5>
                                <p class="small text-muted font-italic">{{$barangs->deskripsi_barang}}</p>
                                <ul class="list-inline small">
                                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                    <li class="list-inline-item m-0"><i class="fa fa-star-o text-success"></i></li>
                                </ul>
                            </div>
                            <div class="card-footer text-muted">
                            <a href="{{route('barang.detail_barang',$barangs->id)}}" class="btn btn-warning"><i class="fas fa-box-open"></i> Detail</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
</div>
<!-- END: Content-->
@endsection