<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use App\barang;
use App\klaim_barang;
use App\log_barang;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class StatusController extends Controller
{
    public function status_null()
    {
        return view('barang.status_null');
    }
    public function json_null()
    {
        $data=barang::where('status','=',null)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_klaim()
    {
        return view('barang.status_klaim');
    }
    public function json_klaim()
    {
        $data=barang::where('status','=','klaim')->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('tolak', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_status_tolak').'" method="get" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                 <h3>Yakin Menolak Klaim ?</h3>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" hidden class="form-control" name="barang_id" placeholder="Bukti" value="'.$user->id.'">
                                         <div class="form-control-position">
                                         <i class="fa fa-user-o" aria-hidden="true"></i>
                                         </div>
                                     </fieldset>
                                 </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    ';
               })
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_tolak()
    {
        return view('barang.status_tolak');
    }
    public function json_tolak()
    {
        $data=barang::where('status','=','tolak')->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_konfirmasi()
    {
        return view('barang.status_konfirmasi');
    }
    public function json_konfirmasi()
    {
        $data=barang::where('status','=','konfirmasi')->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_kirim()
    {
        return view('barang.status_kirim');
    }
    public function json_kirim()
    {
        $data=barang::where('status','=','kirim')->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_terima()
    {
        return view('barang.status_terima');
    }
    public function json_terima()
    {
        $data=barang::where('status','=','terima')->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    
    //BY PENGGUNA
    public function status_null_user()
    {
        return view('barang.status_null_user');
    }
    public function json_null_user()
    {
        $user=Auth::User()->id;
        $data=barang::where('status','=',null)->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_klaim_user()
    {
        return view('barang.status_klaim_user');
    }
    public function json_klaim_user()
    {
        $user=Auth::User()->id;
        $data=barang::where('status','=','klaim')->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('tolak', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#tolak'.$user->id.'" class="btn btn-danger"><i class="fa fa-close" aria-hidden="true"></i> Tolak</button>
                    <!-- Modal-->
                    <div id="tolak'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_status_tolak').'" method="get" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                 <h3>Yakin Menolak Klaim ?</h3>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" hidden class="form-control" name="barang_id" placeholder="Bukti" value="'.$user->id.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-danger btn-block"><i class="far fa-check-square"></i>Tolak</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#konfirmasi'.$user->id.'" class="btn btn-warning"><i class="fa fa-check" aria-hidden="true"></i> Konfirmasi</button>
                    <!-- Modal-->
                    <div id="konfirmasi'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_status_konfirmasi').'" method="get" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                 <h3>Yakin Konfirmasi ?</h3>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" hidden class="form-control" name="barang_id" value="'.$user->id.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-warning btn-block"><i class="far fa-check-square"></i>Konfirmasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    ';
               })

                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_tolak_user()
    {
        return view('barang.status_tolak_user');
    }
    public function json_tolak_user()
    {
        $user=Auth::User()->id;
        $data=barang::where('status','=','tolak')->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_konfirmasi_user()
    {
        return view('barang.status_konfirmasi_user');
    }
    public function json_konfirmasi_user()
    {
        $user=Auth::User()->id;
        $data=barang::where('status','=','konfirmasi')->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('kirim', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#kirim'.$user->id.'" class="btn btn-info"><i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim</button>
                    <!-- Modal-->
                    <div id="kirim'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_status_kirim').'" method="get" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                             <div class="col-md-12">
                                      <label><b>Resi*</b></label>
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <input type="text" class="form-control" name="resi" placeholder="Nomor Resi" required value="'.$user->resi.'">
                                          <div class="form-control-position">
                                          </div>
                                      </fieldset>
                                  </div>
                                 <div class="col-md-12">
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" hidden class="form-control" name="barang_id" placeholder="Bukti" value="'.$user->id.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-info btn-block"><i class="far fa-check-square"></i>Kirim</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    ';
               })
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_kirim_user()
    {
        return view('barang.status_kirim_user');
    }
    public function json_kirim_user()
    {
        $user=Auth::User()->id;
        $data=barang::with(['log'])->where('status','=','kirim')->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('terima', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#tolak'.$user->id.'" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Terima</button>
                    <!-- Modal-->
                    <div id="tolak'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_status_terima').'" method="get" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                 <h3>Yakin Barang Sudah Diterima ?</h3>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="text" hidden class="form-control" name="barang_id" placeholder="Bukti" value="'.$user->id.'">
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-success btn-block"><i class="far fa-check-square"></i>Terima</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    ';
               })
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function status_terima_user()
    {
        return view('barang.status_terima_user');
    }
    public function json_terima_user()
    {
        $user=Auth::User()->id;
        $data=barang::where('status','=','terima')->where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    
    public function set_status_tolak(Request $request)
    {
        $request->validate([
            'barang_id'=>['required'],
          ]);
        $date=date("Y-m-d");
        $barang_id=$request->get('barang_id');
        $resi=$request->get('resi');
        $status="tolak";
        $barang=barang::find($barang_id);
        $barang->status=$status;
        $barang->save();
        $klaim_barang=klaim_barang::where('barang_id','=',$barang_id)->first();
        $klaim_barang->delete();
        log_barang::create([
            'barang_id' => $barang_id, 
            'tanggal' => $date, 
            'resi' => $resi,
            'status' => $status,
        ]);
        // dd($date);
        return redirect()->back()->with('success', 'Klaim Ditolak');
    }
    public function set_status_konfirmasi(Request $request)
    {
        # code...
        $request->validate([
            'barang_id'=>['required'],
          ]);
        $date=date("Y-m-d");
        $barang_id=$request->get('barang_id');
        $resi=$request->get('resi');
        $status="konfirmasi";
        $barang=barang::find($barang_id);
        $barang->status=$status;
        $barang->save();
        $klaim_barang=klaim_barang::where('barang_id','=',$barang_id)->first();
        $klaim_barang->status=$status;
        $klaim_barang->save();
        log_barang::create([
            'barang_id' => $barang_id, 
            'tanggal' => $date, 
            'resi' => $resi,
            'status' => $status,
        ]);
        // dd($date);
        return redirect()->back()->with('success', 'Klaim Diterima');
    }
    public function set_bukti_tf(Request $request)
    {
        # code...
        $request->validate([
            'barang_id'=>['required'],
            'bukti_tf'=>['required'],
          ]);
        $date=date("Y-m-d");
        $barang_id=$request->get('barang_id');
        $resi=$request->get('resi');
        $status="transfer";
        $foto = $request->file('bukti_tf');
        $path = $foto->store('public/bukti_tf');
        $klaim_barang=klaim_barang::where('barang_id','=',$barang_id)->first();
        $klaim_barang->status=$status;
        $klaim_barang->bukti_tf=$path;
        $klaim_barang->save();
        log_barang::create([
            'barang_id' => $barang_id, 
            'tanggal' => $date, 
            'resi' => $resi,
            'status' => $status,
            'pic' => $path,
        ]);
        // dd($date);
        return redirect()->back()->with('success', 'Diproses Kirim');
    }
    public function set_status_kirim(Request $request)
    {
        # code...
        $request->validate([
            'barang_id'=>['required'],
            'resi'=>['required'],
          ]);
        $date=date("Y-m-d");
        $barang_id=$request->get('barang_id');
        $resi=$request->get('resi');
        $status="kirim";
        $barang=barang::find($barang_id);
        $barang->status=$status;
        $barang->save();
        $klaim_barang=klaim_barang::where('barang_id','=',$barang_id)->first();
        $klaim_barang->status=$status;
        $klaim_barang->save();
        log_barang::create([
            'barang_id' => $barang_id, 
            'tanggal' => $date, 
            'resi' => $resi,
            'status' => $status,
        ]);
        // dd($date);
        return redirect()->back()->with('success', 'Diproses Kirim');
    }
    public function set_status_terima(Request $request)
    {
        # code...
        $request->validate([
            'barang_id'=>['required'],
          ]);
        $date=date("Y-m-d");
        $barang_id=$request->get('barang_id');
        $resi=$request->get('resi');
        $status="terima";
        $barang=barang::find($barang_id);
        $barang->status=$status;
        $barang->save();
        $klaim_barang=klaim_barang::where('barang_id','=',$barang_id)->first();
        $klaim_barang->status=$status;
        $klaim_barang->save();
        log_barang::create([
            'barang_id' => $barang_id, 
            'tanggal' => $date, 
            'resi' => $resi,
            'status' => $status,
        ]);
        // dd($date);
        return redirect()->back()->with('success', 'Terimakasih Paket Diterima');
    }
}
