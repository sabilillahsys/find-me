<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class PasswordController extends Controller
{
    public function data_password()
    {
        return view('password.reset_password');
    }
    public function update_password(Request $request)
        {
            $user_id=Auth::User()->id;
            $data=User::find($user_id);
            $data->password=$request->get('password');
            $data->save();
            return redirect()->back()->with('success', 'Password Berhasil Update');
        }
}
