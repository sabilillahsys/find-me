<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class RegistrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registrasi()
    {
        return view('registrasi');
    }

    public function input_registrasi(Request $request)
    {
        $request->validate([
            'nama'=>['required'],
            'password'=>['required'],
            'hp'=>['required'],
        ]);
        $hp=null;
          if ($request->get('hp')!=null) {
                $hp=$request->get('hp');
                $hp=str_replace(' ', '', $hp);
                $hp=str_replace('-', '', $hp);
                $hp=str_replace('+', '', $hp);
                $cekhp=substr($hp,0,1);
                if($cekhp==0){
                    $hp=substr_replace($hp,"62",0,1);
                }else if($cekhp==8){
                    $hp=substr_replace($hp,"628",0,1);
                }
          }
        User::create([ 
            'name'=>$request->get('nama'), 
            'username'=>$hp, 
            'hp'=>$hp, 
            'password'=>$request->get('password'),
            'role_id'=>2,
            'pic'=>'belum',
            'status'=>'Aktif',
        ]);
        return redirect()->route('login')->with('success', 'Akun Berhasil Dibuat');
    }
}
