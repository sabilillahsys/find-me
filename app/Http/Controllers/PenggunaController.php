<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\barang;
use App\log_barang;
use App\klaim_barang;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class PenggunaController extends Controller
{
    public function index(Request $request)
    {
        $cari=$request->get('q');
        // dd($cari);
        if ($cari!=null) {
            $barang=barang::with(['klaim'])
                            ->where('nama_barang', 'like', '%' . $cari . '%')
                            ->where('status',null)
                            ->orWhere('status','=','tolak')
                            ->orderBy('id','DESC')
                            ->get();
        }else{
            $barang=barang::with(['klaim'])->where('status',null)->orWhere('status','=','tolak')->orderBy('id','DESC')->get();
        }
        // dd($barang->toArray());
        return view('pengguna.beranda',compact('barang'));
    }
    public function data_pengguna()
    {
        return view('pengguna.data_pengguna');
    }
    public function json_pengguna()
    {
        $data=User::where('role_id','=',2)->get();
        //dd($data->toArray());
        return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '
                        <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                        <!-- Modal-->
                        <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                
                                <div class="modal-body">
                                <form class="form-horizontal" action="'.route('pengguna.update_pengguna',$user->id).'" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="'.csrf_token().'">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><b>Nama Lengkap*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="'.$user->name.'">
                                            <div class="form-control-position">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>WhatsApp*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="number" class="form-control" name="hp" placeholder="WhatsApp" required value="'.$user->hp.'">
                                            <div class="form-control-position">
                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Status*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                        <select class="form-control" name="status"  required>
                                        <option value="Aktif" >Aktif</option>
                                        <option value="Nonaktif" >Nonaktif</option>
                                        </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Jenis Kelamin*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                        <select class="form-control" name="jk"  required>
                                        <option value="L" >Laki - Laki</option>
                                        <option value="P" >Perempuan</option>
                                        </select>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Foto Profil*</b></label>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="file" class="form-control" name="pic">
                                            <div class="form-control-position">
                                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-12 pt-2">
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                    <!-- Modal-->
                    <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                                <h4>
                                Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                                </h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                                <a href="'.route('pengguna.delete_pengguna',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                                
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                        ';
                    })
                    ->addColumn('pic', function ($user) {
                        $pic=url('/').Storage::url($user->pic);
                        if ($user->pic==null) {
                            return'Bulum Ada';
                        }else {
                        return'
                        <button type="button" data-toggle="modal" data-target="#pic'.$user->id.'" class="btn btn-info">Gambar</button>
                            <!-- Modal-->
                            <div id="pic'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                                <div role="document" class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                        <img src="'.$pic.'" class="img-fluid" alt="Responsive image">
                                        </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!-- end of modal -->
                        ';
                        }
                    })
                    ->escapeColumns([])
                    ->make(true);
    }

    public function input_pengguna(Request $request)
    {
        $request->validate([
            'name'=>['required'],
            'password'=>['required'],
            'pic'=>'belum',
            'jk'=>['required'],
            'hp'=>['required'],
        ]);
        User::create([ 
            'name'=>$request->get('name'), 
            'username'=>$request->get('hp'), 
            'password'=>$request->get('password'),
            'role_id'=>2,
            'pic'=>'belum',
            'status'=>'Aktif',
            'jk'=>$request->get('jk'),
            'hp'=>$request->get('hp'),
        ]);
        return redirect()->back()->with('success', 'Akun Berhasil Dibuat');
    }
    public function update_pengguna(Request $request, $id)
        {
            $foto = $request->file('pic');
            $path = $foto->store('public/profil');
            $data=User::find($id);
            $data->name=$request->get('name');
            $data->hp=$request->get('hp');
            $data->status=$request->get('status');
            $data->jk=$request->get('jk');
            $data->pic=$path;
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }
    public function delete_pengguna($id)
    {
        $data=User::find($id);
            Storage::delete($data->pic);
            $data->delete();
            return redirect()->back()->with('success', 'Berhasil dihapus');
        }
        public function api(Request $request)
        {
            if ($request->has('q')) {
                $cari = $request->q;
                $data=User::where('role_id','=',2)->where('name','LIKE','%'.$cari.'%')->get();
                return response()->json($data);
            }
        
        }
        //profil
    public function data_profil_pengguna()
    {
        return view('pengguna.profil_pengguna');
    }
    public function data_profil()
    {
        return view('pengguna.data_profil');
    }
    public function update_profil(Request $request)
        {
            $user_id=Auth::User()->id;
            $data=User::find($user_id);
            $data->name=$request->get('name');
            $data->jk=$request->get('jk');
            $data->kota=$request->get('kota');
            $data->kecamatan=$request->get('kecamatan');
            $data->alamat=$request->get('alamat');
            $data->save();
            return redirect()->back()->with('success', 'Akun Berhasil Update');
        }
        public function update_foto_profil(Request $request)
        {
            $request->validate([
                'pic' => ['image','mimes:jpeg,png,jpg,gif,svg']
              ]);
              $user_id=Auth::User()->id;
                $data=User::find($user_id);
                $foto = $request->file('pic');
                $path = $foto->store('public/profil');
                Storage::delete($data->pic);
                $data->pic=$path;
                $data->save();
            return redirect()->back()->with('success', 'Berhasil Diperbaharui');
        }
}
