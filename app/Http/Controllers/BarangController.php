<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\log;
use App\barang;
use App\klaim_barang;
use App\log_barang;
use Auth;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class BarangController extends Controller
{
    public function data_barang()
    {
        return view('barang.data_barang');
    }
    public function json_barang()
    {
        $data=barang::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.update_barang',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                             <div class="col-md-6">
                             <label><b>Nama Barang*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" value="'.$user->nama_barang.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-user-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Kota*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="kota" placeholder="Kota" required value="'.$user->kota.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Kecamatan*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" required value="'.$user->kecamatan.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Alamat*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="alamat" placeholder="Alamat" required value="'.$user->alamat.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Tanggal*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="tanggal" placeholder="Alamat" required value="'.$user->tanggal.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Harga*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="harga" placeholder="Harga" required value="'.$user->harga.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 1*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic1" value="'.$user->pic1.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 2*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic2" value="'.$user->pic2.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 3*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic3" value="'.$user->pic3.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Perbaharui</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('barang.delete_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('klaim_barang', function ($user) {
                    return '<a href="'.route('klaim_barang.data_klaim_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-warning">Klaim</a>';
                })
                ->addColumn('log_barang', function ($user) {
                    return '<a href="'.route('log_barang.data_log_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-success">Log</a>';
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    
    public function data_user_barang()
    {
        return view('barang.data_user_barang');
    }
    public function json_user_barang()
    {
        $user=Auth::User()->id;
        $data=barang::where('user_id','=',$user)->get();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.update_barang',$user->id).'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                             <div class="col-md-6">
                             <label><b>Nama Barang*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" value="'.$user->nama_barang.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-user-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Kota*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="kota" placeholder="Kota" required value="'.$user->kota.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Kecamatan*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" required value="'.$user->kecamatan.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Alamat*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="alamat" placeholder="Alamat" required value="'.$user->alamat.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Tanggal*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="tanggal" placeholder="Alamat" required value="'.$user->tanggal.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>Harga*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="text" class="form-control" name="harga" placeholder="Harga" required value="'.$user->harga.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 1*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic1" value="'.$user->pic1.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 2*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic2" value="'.$user->pic2.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                         <div class="col-md-6">
                             <label><b>PIC 3*</b></label>
                             <fieldset class="form-group position-relative has-icon-left">
                                 <input type="file" class="form-control" name="pic3" value="'.$user->pic3.'">
                                 <div class="form-control-position">
                                 <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                 </div>
                             </fieldset>
                         </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Perbaharui</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                <!-- Modal-->
                <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                        
                        <div class="modal-body">
                            <h4>
                            Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                            </h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            <a href="'.route('barang.delete_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i><i class="fa fa-trash" aria-hidden="true"></i> Hapus</a>
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- end of modal -->
                    ';
                })
                ->addColumn('pic1', function ($user) {
                    $pic1=url('/').Storage::url($user->pic1);
                    if ($user->pic1==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic1.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic2', function ($user) {
                    $pic2=url('/').Storage::url($user->pic2);
                    if ($user->pic2==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic2'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic2'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic2.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pic3', function ($user) {
                    $pic3=url('/').Storage::url($user->pic3);
                    if ($user->pic3==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic3'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic3'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic3.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('penemu', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('klaim_barang', function ($user) {
                    return '<a href="'.route('klaim_barang.data_klaim_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-warning">Klaim</a>';
                })
                ->addColumn('log_barang', function ($user) {
                    return '<a href="'.route('log_barang.data_log_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-success">Log</a>';
                })
                ->addColumn('lokasi', function ($user) {
                    return ' <iframe 
                    class="embed-responsive-item"  
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q='.$user->lt.','.$user->lg.'&hl=es&z=14&amp;output=embed">
                    </iframe>';
                })
                ->escapeColumns([])
                ->make(true);
    }
    public function input_barang(Request $request)
    {
        $request->validate([
            'nama_barang'=>['required'],
            'kota'=>['required'],
            'kecamatan'=>['required'],
            'alamat'=>['required'],
            'tanggal'=>['required'],
            'lt'=>['required'],
            'lg'=>['required'],
            'harga'=>['required'],
            'pic1'=>['required'],
          ]);
          $user_id=$request->get('user_id');
          if ($user_id==null) {
            $user_id=Auth::User()->id;
          }
            $foto1 = $request->file('pic1');
            $path1 = $foto1->store('public/barang');
            $foto2 = $request->file('pic2');
            $path2 = null;
            $path3 = null;
            if ($foto2!=null) {
                $path2 = $foto2->store('public/barang1');
            }
            $foto3 = $request->file('pic3');
            if ($foto3!=null) {
                $path3 = $foto3->store('public/barang2');
            }
            barang::create([
            'user_id' => $user_id, 
            'nama_barang' => $request->get('nama_barang'), 
            'deskripsi_barang' => $request->get('deskripsi_barang'), 
            'kota' => $request->get('kota'),
            'kecamatan' => $request->get('kecamatan'),
            'alamat' => $request->get('alamat'),
            'status' => null,
            'tanggal' => $request->get('tanggal'),
            'lt' => $request->get('lt'),
            'lg' => $request->get('lg'),
            'harga' => $request->get('harga'),
            'pic1' => $path1,
            'pic2' => $path2,
            'pic3' => $path3,
        ]);
        return redirect()->back()->with('success', 'Data Berhasil ditambahkan');
    }
    public function update_barang(Request $request, $id)
    {
            $data=barang::find($id);
            $data->nama_barang=$request->get('nama_barang');
            $data->deskripsi_barang=$request->get('deskripsi_barang');
            $data->kota=$request->get('kota');
            $data->kecamatan=$request->get('kecamatan');
            $data->alamat=$request->get('alamat');
            $data->harga=$request->get('harga');
            $data->tanggal=$request->get('tanggal');
            $foto = $request->file('pic1');
            if ($foto!=null) {
                $path = $foto->store('public/barang');
                $data->pic1=$path;
            }
            $foto2 = $request->file('pic2');
            if ($foto2!=null) {
                $path2 = $foto2->store('public/barang1');
                $data->pic2=$path2;
            }
            $foto3 = $request->file('pic3');
            if ($foto3!=null) {
                $path3 = $foto3->store('public/barang2');
                $data->pic3=$path3;
            }
            $data->save();
        return redirect()->back()->with('success', 'Berhasil Diperbaharui');
    }
   
    public function delete_barang($id)
    {
        $data=barang::find($id);
        Storage::delete($data->pic);
        $data->delete();
        return redirect()->back()->with('success', 'Berhasil Dihapus');
    }
    //klaim barang
    public function data_klaim_barang($id)
    {
    return view('barang.data_klaim_barang',compact('id'));
    }
    public function json_klaim_barang($id)
     {
     $data=klaim_barang::with(['user'])->where('barang_id','=',$id)->get();

         return Datatables::of($data)
                 ->addIndexColumn()
                 ->addColumn('action', function ($user) {
                     return '
                     <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                     <!-- Modal-->
                     <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                         <div role="document" class="modal-dialog">
                             <div class="modal-content">
                             
                             <div class="modal-body">
                             <form class="form-horizontal" action="'.route('klaim_barang.update_klaim_barang',$user->id).'" method="post" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="'.csrf_token().'">
                              <div class="row">
                                  <div class="col-md-6">
                                      <label><b>Bukti*</b></label>
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <input type="text" class="form-control" name="bukti" placeholder="Bukti" value="'.$user->bukti.'">
                                          <div class="form-control-position">
                                          <i class="fa fa-user-o" aria-hidden="true"></i>
                                          </div>
                                      </fieldset>
                                  </div>
                                  <div class="col-md-6">
                                      <label><b>Deskripsi*</b></label>
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" required value="'.$user->deskripsi.'">
                                          <div class="form-control-position">
                                          <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                          </div>
                                      </fieldset>
                                  </div>
                                  <div class="col-md-6">
                                      <label><b>Status*</b></label>
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <input type="text" class="form-control" name="status" placeholder="Status" required value="'.$user->status.'">
                                          <div class="form-control-position">
                                          <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                          </div>
                                      </fieldset>
                                  </div>
                                  <div class="col-md-6">
                                      <label><b>Bukti TF*</b></label>
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <input type="file" class="form-control" name="bukti_tf"  required value="'.$user->bukti_tf.'">
                                          <div class="form-control-position">
                                          <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                          </div>
                                      </fieldset>
                                  </div>
                                 <div class="col-md-12 pt-2">
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                     </fieldset>
                                 </div>
                             </div>
                         </form>
                             </div>
                             <div class="modal-footer">
                                 <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                             </div>
                             </div>
                         </div>
                         </div>
                         <!-- end of modal -->
                 <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                 <!-- Modal-->
                 <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                     <div role="document" class="modal-dialog">
                         <div class="modal-content">
                         
                         <div class="modal-body">
                             <h4>
                             Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                             </h4>
                         </div>
                         <div class="modal-footer">
                             <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                             <a href="'.route('klaim_barang.delete_klaim_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                             
                         </div>
                         </div>
                     </div>
                     </div>
                     <!-- end of modal -->
                     ';
                })
                
                ->addColumn('bukti_tf', function ($user) {
                    $bukti_tf=url('/').Storage::url($user->bukti_tf);
                    if ($user->bukti_tf==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$bukti_tf.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('bukti', function ($user) {
                    $bukti=url('/').Storage::url($user->bukti);
                    if ($user->bukti==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$bukti.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pemilik', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('barang', function ($user) {
                    return $user->barang->nama_barang;
                })
                 ->escapeColumns([])
                 ->make(true);
     }
     public function data_user_klaim_barang()
    {
    return view('barang.data_user_klaim_barang');
    }
     public function json_user_klaim_barang()
     {
        $id=Auth::User()->id;
        $data=klaim_barang::with(['user'])->where('user_id','=',$id)->get();
         return Datatables::of($data)
                 ->addIndexColumn()
                 ->addColumn('tf', function ($user) {
                    return '
                    <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Bukti TF</button>
                    <!-- Modal-->
                    <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                            
                            <div class="modal-body">
                            <form class="form-horizontal" action="'.route('barang.set_bukti_tf').'" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="'.csrf_token().'">
                             <div class="row">
                                 <div class="col-md-12">
                                     <label><b>Bukti TF*</b></label>
                                     <fieldset class="form-group position-relative has-icon-left">
                                         <input type="file" class="form-control" name="bukti_tf"  required>
                                         <div class="form-control-position">
                                         </div>
                                     </fieldset>
                                 </div>
                                 <div class="col-md-12">
                                         <fieldset class="form-group position-relative has-icon-left">
                                             <input type="text" hidden class="form-control" name="barang_id" value="'.$user->barang_id.'">
                                             <div class="form-control-position">
                                             </div>
                                         </fieldset>
                                     </div>
                                <div class="col-md-12 pt-2">
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                            </div>
                            </div>
                        </div>
                        </div>
                        <!-- end of modal -->
                    ';
               })
                ->addColumn('bukti_tf', function ($user) {
                    $bukti_tf=url('/').Storage::url($user->bukti_tf);
                    if ($user->bukti_tf==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic1'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic1'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$bukti_tf.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('bukti', function ($user) {
                    $bukti=url('/').Storage::url($user->bukti);
                    if ($user->bukti==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$bukti.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                ->addColumn('pemilik', function ($user) {
                    return $user->user->name;
                })
                ->addColumn('barang', function ($user) {
                    return $user->barang->nama_barang;
                })
                 ->escapeColumns([])
                 ->make(true);
     }
     public function input_klaim_barang(Request $request)
     {
         $request->validate([
             'bukti'=>['required'],
             'deskripsi'=>['required'],
           ]);
           $user_id=Auth::User()->id;
           $foto = $request->file('bukti');
           $path = $foto->store('public/bukti');
           klaim_barang::create([
             'user_id' => $user_id, 
             'barang_id' => $request->get('barang_id'), 
             'deskripsi' => $request->get('deskripsi'),
             'status' => 'klaim',
             'bukti' => $path,
            ]);
             $barang=$request->get('barang_id');
             $data=barang::find($barang);
             $data->status="klaim";
             $data->save();
             return redirect()->back()->with('success', 'Data Diajukan klaim');
     }
     public function user_klaim_barang(Request $request)
     {
         $request->validate([
             'barang_id'=>['required'],
             'bukti'=>['required'],
             'deskripsi'=>['required'],
           ]);
           $user=Auth::User()->id;
           $foto = $request->file('bukti');
           $path = $foto->store('public/bukti');
           $status="klaim";
           klaim_barang::create([
             'user_id' => $user, 
             'barang_id' => $request->get('barang_id'), 
             'deskripsi' => $request->get('deskripsi'),
             'status' => $status,
             'bukti' => $path,
         ]);
             $barang=$request->get('barang_id');
             $data=barang::find($barang);
             $data->status="klaim";
             $data->save();
         return redirect()->back()->with('success', 'Data Berhasil ditambahkan');
     }
     public function update_user_klaim_barang(Request $request, $id)
     {
        $foto = $request->file('bukti_tf');
        $path = $foto->store('public/bukti_tf');
        $data=klaim_barang::find($id);
        $data->bukti_tf=$path;
         return redirect()->back()->with('success', 'Berhasil Diperbaharui');
     }
    
     public function delete_klaim_barang($id)
     {
         $data=klaim_barang::find($id);
         $data->delete();
         return redirect()->back()->with('success', 'Berhasil Dihapus');
     }
     //LOG BARANG
     public function data_log_barang($id)
     {
     return view('barang.data_log_barang',compact('id'));
     }
     public function json_log_barang($id)
      {
      $data=log_barang::where('barang_id','=',$id)->get();
 
          return Datatables::of($data)
                  ->addIndexColumn()
                  ->addColumn('action', function ($user) {
                      return '
                      <button type="button" data-toggle="modal" data-target="#update'.$user->id.'" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i>Update</button>
                      <!-- Modal-->
                      <div id="update'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                          <div role="document" class="modal-dialog">
                              <div class="modal-content">
                              
                              <div class="modal-body">
                              <form class="form-horizontal" action="'.route('log_barang.update_log_barang',$user->id).'" method="post" enctype="multipart/form-data">
                              <input type="hidden" name="_token" value="'.csrf_token().'">
                               <div class="row">
                                   <div class="col-md-6">
                                       <label><b>Status*</b></label>
                                       <fieldset class="form-group position-relative has-icon-left">
                                           <input type="text" class="form-control" name="status" placeholder="Status" required value="'.$user->status.'">
                                           <div class="form-control-position">
                                           <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                           </div>
                                       </fieldset>
                                   </div>
                                   <div class="col-md-6">
                                       <label><b>PIC*</b></label>
                                       <fieldset class="form-group position-relative has-icon-left">
                                           <input type="file" class="form-control" name="pic"  required value="'.$user->pic.'">
                                           <div class="form-control-position">
                                           <i class="fa fa-address-book-o" aria-hidden="true"></i>
                                           </div>
                                       </fieldset>
                                   </div>
                                  <div class="col-md-12 pt-2">
                                      <fieldset class="form-group position-relative has-icon-left">
                                          <button type="submit" class="btn btn-outline-primary btn-block"><i class="far fa-check-square"></i> Verifikasi</button>
                                      </fieldset>
                                  </div>
                              </div>
                          </form>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                              </div>
                              </div>
                          </div>
                          </div>
                          <!-- end of modal -->
                  <button type="button" data-toggle="modal" data-target="#hapus'.$user->id.'" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i>Hapus</button>
                  <!-- Modal-->
                  <div id="hapus'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                      <div role="document" class="modal-dialog">
                          <div class="modal-content">
                          
                          <div class="modal-body">
                              <h4>
                              Yakin Akan Mengapus <b>'.$user->nama_lengkap.'</b>?
                              </h4>
                          </div>
                          <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-xs btn-info">Batal</button>
                              <a href="'.route('log_barang.delete_log_barang',$user->id).'" data-id="'.$user->id.'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Hapus</a>
                              
                          </div>
                          </div>
                      </div>
                      </div>
                      <!-- end of modal -->
                      ';
                 }) 
                 ->addColumn('pic', function ($user) {
                    $pic=url('/').Storage::url($user->pic);
                    if ($user->pic==null) {
                        return'Bulum Ada';
                    }else {
                    return'
                    <button type="button" data-toggle="modal" data-target="#pic'.$user->id.'" class="btn btn-info">Gambar</button>
                        <!-- Modal-->
                        <div id="pic'.$user->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <img src="'.$pic.'" class="img-fluid" alt="Responsive image">
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-xs btn-warning">Tutup</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!-- end of modal -->
                    ';
                    }
                })
                 ->addColumn('barang', function ($user) {
                     return $user->barang->nama_barang;
                 })
                  ->escapeColumns([])
                  ->make(true);
      }
      public function input_log_barang(Request $request)
      {
          $request->validate([
              'barang_id'=>['required'],
              'tanggal'=>['required'],
              'resi'=>['required'],
              'status'=>['required'],
              'pic'=>['required'],
            ]);
            $foto = $request->file('pic');
            $path = $foto->store('public/log_barang');
            log_barang::create([
              'barang_id' => $request->get('barang_id'), 
              'tanggal' => $request->get('tanggal'), 
              'resi' => $request->get('resi'),
              'status' => $request->get('status'),
              'pic' => $path,
          ]);
          return redirect()->back()->with('success', 'Data Berhasil ditambahkan');
      }
      public function update_log_barang(Request $request, $id)
      {
         $foto = $request->file('pic');
         $path = $foto->store('public/log_barang');
              $data=log_barang::find($id);
              $data->tanggal=$request->get('tanggal');
              $data->resi=$request->get('resi');
              $data->status=$request->get('status');
              $data->pic=$path;
          return redirect()->back()->with('success', 'Berhasil Diperbaharui');
      }
     
      public function delete_log_barang($id)
      {
          $data=log_barang::find($id);
          Storage::delete($data->pic);
          $data->delete();
          return redirect()->back()->with('success', 'Berhasil Dihapus');
      }
      public function detail_barang($id)
      {
        $data=barang::with(['user','log'])->find($id);
          return view('barang.detail_barang',compact('data'));
      }
}
