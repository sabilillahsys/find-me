<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    //
    protected $fillable = [
            'user_id',
            'nama_barang',
            'deskripsi_barang',
            'kota',
            'kecamatan',
            'alamat',
            'tanggal',
            'lt',
            'lg',
            'harga',
            'pic1',
            'pic2',
            'pic3',
            'status',
    ];
            
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function klaim()
    {
        return $this->hasMany('App\klaim_barang');
    }
    public function log()
    {
        return $this->hasMany('App\log_barang');
    }
}
