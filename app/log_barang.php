<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log_barang extends Model
{
    //
    protected $fillable = [
        'barang_id',
        'tanggal',
        'resi',
        'pic',
        'status',
    ];
    
    public function barang()
    {
        return $this->belongsTo('App\barang','barang_id');
    }
}
