<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class klaim_barang extends Model
{
    //
    protected $fillable = [
            'user_id',
            'barang_id',
            'bukti',
            'deskripsi',
            'status',
            'bukti_tf',
    ];
            
    public function barang()
    {
        return $this->belongsTo('App\barang','barang_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
