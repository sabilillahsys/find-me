<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('CASCADE')
                  ->onUpdate('cascade');
            $table->text('nama_barang');
            $table->text('deskripsi_barang');
            $table->text('kota');
            $table->text('kecamatan');
            $table->text('alamat');
            $table->date('tanggal');
            $table->text('lt');
            $table->text('lg');
            $table->integer('harga');
            $table->text('pic1')->nullable();
            $table->text('pic2')->nullable();
            $table->text('pic3')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
