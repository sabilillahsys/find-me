<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKlaimBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('klaim_barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('CASCADE')
                  ->onUpdate('cascade');
            $table->integer('barang_id')->unsigned();
            $table->foreign('barang_id')
                  ->references('id')
                  ->on('barangs')
                  ->onDelete('CASCADE')
                  ->onUpdate('cascade');
            $table->text('bukti')->nullable();
            $table->text('deskripsi')->nullable();
            $table->text('status')->nullable();
            $table->text('bukti_tf')->nullable();
            $table->date('tanggal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('klaim_barangs');
    }
}
