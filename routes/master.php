<?php
//Master
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'master','as' => 'master.',
        ], function(){
            Route::get('/','MasterController@index')->name('master');
            //akun master
            Route::get('/data_master','MasterController@data_master')->name('data_master');
            Route::get('/json_master','MasterController@json_master')->name('json_master');
            Route::post('/input_master','MasterController@input_master')->name('input_master');
            Route::post('/update_master/{id}','MasterController@update_master')->name('update_master');
            Route::get('/delete_master/{id}','MasterController@delete_master')->name('delete_master');
            
        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pelanggan']],function(){
        Route::group([
            'prefix' => 'pelanggan','as' => 'pelanggan.',
        ], function(){
            Route::get('/','PelangganController@index')->name('pelanggan');
            //akun master
            Route::get('/api','PelangganController@api')->name('api');

            Route::get('/data_pelanggan','PelangganController@data_pelanggan')->name('data_pelanggan');
            Route::get('/json_pelanggan','PelangganController@json_pelanggan')->name('json_pelanggan');
            Route::post('/input_pelanggan','PelangganController@input_pelanggan')->name('input_pelanggan');
            Route::post('/update_pelanggan/{id}','PelangganController@update_pelanggan')->name('update_pelanggan');
            Route::get('/delete_pelanggan/{id}','PelangganController@delete_pelanggan')->name('delete_pelanggan');
            
        });
    });
});