<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('login', function () {
    return view('login');
})->name('login');
include('master.php');
include('pengguna.php');
include('barang.php');
include('password.php');
include('profil.php');
Route::post('masuk', 'LoginController@masuk')->name('masuk');
Route::get('keluar', 'LoginController@keluar')->name('keluar');
Route::get('/registrasi','RegistrasiController@registrasi')->name('registrasi');
Route::post('/input_registrasi','RegistrasiController@input_registrasi')->name('input_registrasi');