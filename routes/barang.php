<?php
//Master
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pengguna']],function(){
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            //akun barang
            Route::get('/data_barang','BarangController@data_barang')->name('data_barang');
            Route::get('/json_barang','BarangController@json_barang')->name('json_barang');
            Route::get('/data_user_barang','BarangController@data_user_barang')->name('data_user_barang');
            Route::get('/json_user_barang','BarangController@json_user_barang')->name('json_user_barang');
            Route::post('/input_barang','BarangController@input_barang')->name('input_barang');
            Route::post('/update_barang/{id}','BarangController@update_barang')->name('update_barang');
            Route::get('/delete_barang/{id}','BarangController@delete_barang')->name('delete_barang');
            
        });

                Route::group([
                    'prefix' => 'barang','as' => 'barang.',
                ], function(){
                    //akun barang
                    Route::get('/status_null','StatusController@status_null')->name('status_null');
                    Route::get('/json_null','StatusController@json_null')->name('json_null');
                    Route::get('/status_kirim','StatusController@status_kirim')->name('status_kirim');
                    Route::get('/json_kirim','StatusController@json_kirim')->name('json_kirim');
                    Route::get('/status_klaim','StatusController@status_klaim')->name('status_klaim');
                    Route::get('/json_klaim','StatusController@json_klaim')->name('json_klaim');
                    Route::get('/status_tolak','StatusController@status_tolak')->name('status_tolak');
                    Route::get('/json_tolak','StatusController@json_tolak')->name('json_tolak');
                    Route::get('/status_konfirmasi','StatusController@status_konfirmasi')->name('status_konfirmasi');
                    Route::get('/json_konfirmasi','StatusController@json_konfirmasi')->name('json_konfirmasi');
                    Route::get('/status_terima','StatusController@status_terima')->name('status_terima');
                    Route::get('/json_terima','StatusController@json_terima')->name('json_terima');

                });
                Route::group([
                    'prefix' => 'barang','as' => 'barang.',
                ], function(){
                    //akun barang
                    Route::get('/status_null_user','StatusController@status_null_user')->name('status_null_user');
                    Route::get('/json_null_user','StatusController@json_null_user')->name('json_null_user');
                    Route::get('/status_kirim_user','StatusController@status_kirim_user')->name('status_kirim_user');
                    Route::get('/json_kirim_user','StatusController@json_kirim_user')->name('json_kirim_user');
                    Route::get('/status_klaim_user','StatusController@status_klaim_user')->name('status_klaim_user');
                    Route::get('/json_klaim_user','StatusController@json_klaim_user')->name('json_klaim_user');
                    Route::get('/status_tolak_user','StatusController@status_tolak_user')->name('status_tolak_user');
                    Route::get('/json_tolak_user','StatusController@json_tolak_user')->name('json_tolak_user');
                    Route::get('/status_konfirmasi_user','StatusController@status_konfirmasi_user')->name('status_konfirmasi_user');
                    Route::get('/json_konfirmasi_user','StatusController@json_konfirmasi_user')->name('json_konfirmasi_user');
                    Route::get('/status_terima_user','StatusController@status_terima_user')->name('status_terima_user');
                    Route::get('/json_terima_user','StatusController@json_terima_user')->name('json_terima_user');

                });
                Route::group([
                    'prefix' => 'barang','as' => 'barang.',
                ], function(){
                    //akun barang
                    Route::get('/set_status_null','StatusController@set_status_null')->name('set_status_null');
                    Route::get('/set_status_kirim','StatusController@set_status_kirim')->name('set_status_kirim');
                    Route::get('/set_status_klaim','StatusController@set_status_klaim')->name('set_status_klaim');
                    Route::get('/set_status_tolak','StatusController@set_status_tolak')->name('set_status_tolak');
                    Route::get('/set_status_konfirmasi','StatusController@set_status_konfirmasi')->name('set_status_konfirmasi');
                    Route::get('/set_status_terima','StatusController@set_status_terima')->name('set_status_terima');
                    Route::post('/set_bukti_tf','StatusController@set_bukti_tf')->name('set_bukti_tf');
                });

        
        //by user
        Route::group([
            'prefix' => 'klaim_barang','as' => 'klaim_barang.',
        ], function(){
            //akun barang
            Route::get('/data_klaim_barang/{id}','BarangController@data_klaim_barang')->name('data_klaim_barang');
            Route::get('/data_user_klaim_barang','BarangController@data_user_klaim_barang')->name('data_user_klaim_barang');
            Route::get('/json_klaim_barang/{id}','BarangController@json_klaim_barang')->name('json_klaim_barang');
            Route::get('/json_user_klaim_barang','BarangController@json_user_klaim_barang')->name('json_user_klaim_barang');
            Route::post('/input_klaim_barang','BarangController@input_klaim_barang')->name('input_klaim_barang');
            Route::post('/user_klaim_barang','BarangController@user_klaim_barang')->name('user_klaim_barang');
            Route::post('/update_klaim_barang/{id}','BarangController@update_klaim_barang')->name('update_klaim_barang');
            Route::post('/update_user_klaim_barang/{id}','BarangController@update_user_klaim_barang')->name('update_user_klaim_barang');
            Route::get('/delete_klaim_barang/{id}','BarangController@delete_klaim_barang')->name('delete_klaim_barang');
            
        });

        Route::group([
            'prefix' => 'log_barang','as' => 'log_barang.',
        ], function(){
            //akun barang
            Route::get('/data_log_barang/{id}','BarangController@data_log_barang')->name('data_log_barang');
            Route::get('/json_log_barang/{id}','BarangController@json_log_barang')->name('json_log_barang');
            Route::post('/input_log_barang','BarangController@input_log_barang')->name('input_log_barang');
            Route::post('/update_log_barang/{id}','BarangController@update_log_barang')->name('update_log_barang');
            Route::get('/delete_log_barang/{id}','BarangController@delete_log_barang')->name('delete_log_barang');
            
        });
        
        Route::group([
            'prefix' => 'barang','as' => 'barang.',
        ], function(){
            //akun

            Route::get('/detail_barang/{id}','BarangController@detail_barang')->name('detail_barang');

        });
    });
});