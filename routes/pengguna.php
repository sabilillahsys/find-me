<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pengguna']],function(){
        Route::group([
            'prefix' => 'pengguna','as' => 'pengguna.',
        ], function(){
            Route::get('/','PenggunaController@index')->name('pengguna');
            //akun master
            Route::get('/api','PenggunaController@api')->name('api');

            Route::get('/data_pengguna','PenggunaController@data_pengguna')->name('data_pengguna');
            Route::get('/json_pengguna','PenggunaController@json_pengguna')->name('json_pengguna');
            Route::post('/input_pengguna','PenggunaController@input_pengguna')->name('input_pengguna');
            Route::post('/update_pengguna/{id}','PenggunaController@update_pengguna')->name('update_pengguna');
            Route::get('/delete_pengguna/{id}','PenggunaController@delete_pengguna')->name('delete_pengguna');
            
        });
    });
});