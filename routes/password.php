<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pengguna']],function(){
        Route::group([
            'prefix' => 'password','as' => 'password.',
        ], function(){
            Route::get('/data_password','PasswordController@data_password')->name('data_password');
            Route::post('/update_password','PasswordController@update_password')->name('update_password');

        });
    });
});