<?php
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pengguna']],function(){
        Route::group([
            'prefix' => 'profil','as' => 'profil.',
        ], function(){
            //akun

            Route::get('/data_profil_pengguna','PenggunaController@data_profil_pengguna')->name('data_profil_pengguna');

        });
    });
});

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Pengguna']],function(){
        Route::group([
            'prefix' => 'profil','as' => 'profil.',
        ], function(){
            //akun

            Route::get('/data_profil','PenggunaController@data_profil')->name('data_profil');
            Route::post('/update_profil','PenggunaController@update_profil')->name('update_profil');
           Route::post('/update_foto_profil','PenggunaController@update_foto_profil')->name('update_foto_profil');

        });
    });
});